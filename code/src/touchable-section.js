import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Alert} from 'react-native';
import styles from './styles';

export default class TouchableToggleSection extends Component {
  state = {
    showing: true,
  };
  doOnPress = () => {
    // Display the alert
    Alert.alert('Hey dude');

    // toggle hide/show

    /*this.setState({
        showing : !this.state.showing
    })*/

    // https://reactjs.org/docs/react-component.html#setstate
    this.setState((state) => {
      return {showing: !state.showing};
    });
  };

  isTypeOfString = (description) => typeof description === 'string'

  renderDescription = description => {

    // le type de valeur est important car un composant Text ne sait afficher que du texte.
    // si on lui passe autre chose ex: <ReloadInstructions /> il ne saura pas forcément le gérer

    return this.isTypeOfString(description) ? (
      <Text style={styles.sectionDescription}>{description}</Text> // https://facebook.github.io/react-native/docs/text.html
    ) : (
      <View style={styles.sectionDescription}>{description}</View>
    );
  };

  render() {
    const {title, description} = this.props;
    const {showing} = this.state
    return (
      <TouchableOpacity
        style={styles.sectionContainer}
        onPress={this.doOnPress}>
        <Text style={styles.sectionTitle}>{title}</Text>
        {showing && this.renderDescription(description)}
      </TouchableOpacity>
    );
  }
}
