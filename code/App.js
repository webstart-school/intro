/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
} from 'react-native';

import {
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import TouchableSection  from './src/touchable-section';
import styles from "./src/styles"


export default class App extends Component {
  render(){
    return(
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <TouchableSection title={'Step One'} description={
            <Text style={styles.sectionDescription}>
              Edit <Text style={styles.highlight}>App.js</Text> to change this
              screen and then come back to see your edits.
            </Text>} />
          <TouchableSection title={'See Your Changes'} description={<ReloadInstructions />} />
          <TouchableSection title={'Debug'} description={<ReloadInstructions />} />
          <TouchableSection title={'Learn More'} description={'Read the docs to discover what to do next:'} />
        </ScrollView>
      </SafeAreaView>
    )
  }
}
